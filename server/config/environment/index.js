'use strict';

var path = require('path');
var _ = require('lodash');

function requiredProcessEnv(name) {
  if (!process.env[name]) {
    throw new Error('You must set the ' + name + ' environment variable');
  }
  return process.env[name];
}

// All configurations will extend these options
// ============================================
var all = {
  env: process.env.NODE_ENV,

  // Root path of server
  root: path.normalize(__dirname + '/../../..'),

  // Server port
  port: process.env.PORT || 9000,

  // Server IP
  ip: process.env.IP || '0.0.0.0',

  // Should we populate the DB with sample data?
  seedDB: false,

  // Secret for session, you will want to change this and make it an environment variable
  secrets: {
    session: 'greenberry-secret'
  },

  // List of user roles
  userRoles: ['guest', 'user', 'admin'],

  // MongoDB connection options
  mongo: {
    options: {
      db: {
        safe: true
      }
    }
  },

  facebook: {
    clientID:    '1177385945610060',
    clientSecret:  '930d25bb0ee9839336b76e49800fd66e',
    callbackURL:  'http://greenberry-greenberry.rhcloud.com/auth/facebook/callback'
  },

  twitter: {
    clientID:     'RjL5lrWG3QBSe9hMfQWBI9NwV',
    clientSecret: 'bL4qGfdJAcWVSrUkHj2khJVy3xZur728NnUqFDUaFbTJl7BkMU',
    callbackURL:  'http://greenberry-greenberry.rhcloud.com/auth/twitter/callback'
  },

  google: {
    clientID:     '85913046935-43aomq4v597rl4ia9vlb6a1tpckmc421.apps.googleusercontent.com',
    clientSecret: 'q0SmZ1mNEl9WUC9V2NjiZEKM',
    callbackURL:  'http://greenberry-greenberry.rhcloud.com/auth/google/callback'
  }
};

// Export the config object based on the NODE_ENV
// ==============================================
module.exports = _.merge(
  all,
  require('./' + process.env.NODE_ENV + '.js') || {});
