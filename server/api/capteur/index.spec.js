'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var capteurCtrlStub = {
  index: 'capteurCtrl.index',
  show: 'capteurCtrl.show',
  create: 'capteurCtrl.create',
  update: 'capteurCtrl.update',
  destroy: 'capteurCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var capteurIndex = proxyquire('./index.js', {
  'express': {
    Router: function() {
      return routerStub;
    }
  },
  './capteur.controller': capteurCtrlStub
});

describe('Capteur API Router:', function() {

  it('should return an express router instance', function() {
    capteurIndex.should.equal(routerStub);
  });

  describe('GET /api/capteurs', function() {

    it('should route to capteur.controller.index', function() {
      routerStub.get
        .withArgs('/', 'capteurCtrl.index')
        .should.have.been.calledOnce;
    });

  });

  describe('GET /api/capteurs/:id', function() {

    it('should route to capteur.controller.show', function() {
      routerStub.get
        .withArgs('/:id', 'capteurCtrl.show')
        .should.have.been.calledOnce;
    });

  });

  describe('POST /api/capteurs', function() {

    it('should route to capteur.controller.create', function() {
      routerStub.post
        .withArgs('/', 'capteurCtrl.create')
        .should.have.been.calledOnce;
    });

  });

  describe('PUT /api/capteurs/:id', function() {

    it('should route to capteur.controller.update', function() {
      routerStub.put
        .withArgs('/:id', 'capteurCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('PATCH /api/capteurs/:id', function() {

    it('should route to capteur.controller.update', function() {
      routerStub.patch
        .withArgs('/:id', 'capteurCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('DELETE /api/capteurs/:id', function() {

    it('should route to capteur.controller.destroy', function() {
      routerStub.delete
        .withArgs('/:id', 'capteurCtrl.destroy')
        .should.have.been.calledOnce;
    });

  });

});
