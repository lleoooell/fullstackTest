/**
 * Capteur model events
 */

'use strict';

var EventEmitter = require('events').EventEmitter;
var Capteur = require('./capteur.model');
var CapteurEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
CapteurEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  Capteur.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    CapteurEvents.emit(event + ':' + doc._id, doc);
    CapteurEvents.emit(event, doc);
  }
}

module.exports = CapteurEvents;
