'use strict';

var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var Schema = mongoose.Schema;

var CapteurSchema = new Schema({
    name: String,
    MCP: Number,
    TCA:Number,
    HUMA:Number,
    LUM:Number,
    DUST:Number,
    BAT:Number,
    created: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Capteur', CapteurSchema);