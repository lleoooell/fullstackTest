'use strict';

var app = require('../..');
var request = require('supertest');

var newCapteur;

describe('Capteur API:', function() {

  describe('GET /api/capteurs', function() {
    var capteurs;

    beforeEach(function(done) {
      request(app)
        .get('/api/capteurs')
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          capteurs = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      capteurs.should.be.instanceOf(Array);
    });

  });

  describe('POST /api/capteurs', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/capteurs')
        .send({
          name: 'New Capteur',
          info: 'This is the brand new capteur!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          newCapteur = res.body;
          done();
        });
    });

    it('should respond with the newly created capteur', function() {
      newCapteur.name.should.equal('New Capteur');
      newCapteur.info.should.equal('This is the brand new capteur!!!');
    });

  });

  describe('GET /api/capteurs/:id', function() {
    var capteur;

    beforeEach(function(done) {
      request(app)
        .get('/api/capteurs/' + newCapteur._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          capteur = res.body;
          done();
        });
    });

    afterEach(function() {
      capteur = {};
    });

    it('should respond with the requested capteur', function() {
      capteur.name.should.equal('New Capteur');
      capteur.info.should.equal('This is the brand new capteur!!!');
    });

  });

  describe('PUT /api/capteurs/:id', function() {
    var updatedCapteur

    beforeEach(function(done) {
      request(app)
        .put('/api/capteurs/' + newCapteur._id)
        .send({
          name: 'Updated Capteur',
          info: 'This is the updated capteur!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedCapteur = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedCapteur = {};
    });

    it('should respond with the updated capteur', function() {
      updatedCapteur.name.should.equal('Updated Capteur');
      updatedCapteur.info.should.equal('This is the updated capteur!!!');
    });

  });

  describe('DELETE /api/capteurs/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/api/capteurs/' + newCapteur._id)
        .expect(204)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when capteur does not exist', function(done) {
      request(app)
        .delete('/api/capteurs/' + newCapteur._id)
        .expect(404)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
