'use strict';

var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var Schema = mongoose.Schema;

var ActionSchema = new Schema({
  name: String,
  led1: Boolean,
  led2:Boolean,
  led3:Number
});

module.exports = mongoose.model('Action', ActionSchema);
