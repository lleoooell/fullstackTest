/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/pictures              ->  index
 * POST    /api/pictures              ->  create
 * GET     /api/pictures/:id          ->  show
 * PUT     /api/pictures/:id          ->  update
 * DELETE  /api/pictures/:id          ->  destroy
 */

'use strict';

var _ = require('lodash');
var Picture = require('./picture.model');

var path = require('path');

var multiparty = require('multiparty');
var util = require('util');
var uuid = require('node-uuid');
var fs = require('fs');
var localConfig = require('../../config/local.env');
var mv = require('mv');
function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

function responseWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function saveUpdates(updates) {
  return function(entity) {
    var updated = _.merge(entity, updates);
    return updated.saveAsync()
      .spread(function(updated) {
        return updated;
      });
  };
}

function removeEntity(res) {
  return function(entity) {
    if (entity) {
      return entity.removeAsync()
        .then(function() {
          res.status(204).end();
        });
    }
  };
}

// Gets a list of Pictures
exports.index = function(req, res) {
  var query = req.query.query && JSON.parse(req.query.query);
  console.log('query is  ' + JSON.stringify(req.query.query));
  Picture.find(query).sort({_id:-1}).limit(5).execAsync()
    .then(responseWithResult(res))
    .catch(handleError(res));
};

// Gets a single Picture from the DB
exports.show = function(req, res) {
  Picture.findByIdAsync(req.params.id)
    .then(handleEntityNotFound(res))
    .then(responseWithResult(res))
    .catch(handleError(res));
};

// Creates a new Picture in the DB
exports.create = function(req, res) {
  var form = new multiparty.Form();

  form.parse(req, function(err, fields, files) {
        var file = files.file[0];
        var contentType = file.headers['content-type'];
        var tmpPath = file.path;
        var extIndex = tmpPath.lastIndexOf('.');
        var extension = (extIndex < 0) ? '' : tmpPath.substr(extIndex);
        // uuid is for generating unique filenames. 
        var fileName = uuid.v4() + extension;
        console.log(localConfig.OPENSHIFT_DATA_DIR);
        var destPath = localConfig.OPENSHIFT_DATA_DIR  + 'pict/' +  fileName;

        // Server side file type checker.
        if (contentType !== 'image/png' && contentType !== 'image/jpeg') {
            fs.unlink(tmpPath);
            return res.status(400).send('Unsupported file type.');
        }

        mv(tmpPath, destPath,{mkdirp: true}, function(err) {
            if (err) {
                return res.status(400).send('Image is not saved:' + err);
            }
            var tmpPict={};
            tmpPict.tag = 'greenberry';
            tmpPict.path = destPath;
            tmpPict.filename = fileName;
            Picture.createAsync(tmpPict)
            .then(responseWithResult(res, 201))
            .catch(handleError(res));
            // return res.json(tmpPict);
        });
    });
};

// Updates an existing Picture in the DB
exports.update = function(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  Picture.findByIdAsync(req.params.id)
    .then(handleEntityNotFound(res))
    .then(saveUpdates(req.body))
    .then(responseWithResult(res))
    .catch(handleError(res));
};

// Deletes a Picture from the DB
exports.destroy = function(req, res) {
  Picture.findByIdAsync(req.params.id)
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
};
