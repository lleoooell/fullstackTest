'use strict';

var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var Schema = mongoose.Schema;

var PictureSchema = new Schema({
  tag: String,
  path:String,
  filename:String,
  creation: {
  	type : Date,
  	default : Date.now()
  }
});



module.exports = mongoose.model('Picture', PictureSchema);
