'use strict';

describe('Controller: UpLoadCtrl', function () {

  // load the controller's module
  beforeEach(module('greenberryApp'));

  var UpLoadCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    UpLoadCtrl = $controller('UpLoadCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
