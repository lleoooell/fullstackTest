'use strict';

angular.module('greenberryApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('upLoad', {
        url: '/upLoad',
        templateUrl: 'app/upLoad/upLoad.html',
        controller: 'UpLoadCtrl'
      });
  });
