'use strict';

angular.module('greenberryApp')
  .directive('resize', function () {
    return function (scope, element) {
        var w = window;
        scope.getWindowDimensions = function () {
            return {
                'h': w.innerHeight,
                'w': w.innerWidth
            };
        };
        scope.$watch(scope.getWindowDimensions, function (newValue, oldValue) {
            scope.windowHeight = newValue.h;
            scope.windowWidth = newValue.w;

            scope.style = function () {
                return {
                    'height': (newValue.h - 50) + 'px',
                     'width': (newValue.w) + 'px',


                };
            };
            scope.styleT = function () {
                return {
                    'height': (newValue.h -50) + 'px',
                     'width': (newValue.w) + 'px',
                     'position': 'absolute',
                     'top' : 60,
                     'left' : 0,
                     'overflow' : 'auto',
                };
            };
             scope.style2 = function () {
                return {
                    'height': (newValue.h -160) + 'px',
                     
                };
            };
             scope.styleW = function () {
                return {
                    'width': (newValue.w/3) + 'px',
                    'margin-left':(newValue.w/3) + 'px'
                     
                };
            };
            scope.style8 = function () {
                return {
                    'margin-top': (newValue.h) + 'px'
                        
                };
            };
            scope.style9 = function () {
                return {
                    'height': (newValue.h/1.5 ) + 'px',
                        'width': (newValue.w) + 'px'
                        
                };
            };
             scope.style92 = function () {
                return {
                   
                        'width': (newValue.w) + 'px'
                        
                };
            };
            scope.style99 = function () {
                return {
                    'height': (newValue.h/1.9 ) + 'px',
                        'width': (newValue.w) + 'px'
                        
                };
            };
            scope.styleMap = function () {
                return {
                    
                        'width': (newValue.w) + 'px'
                        
                };
            };
             scope.style10 = function () {
                return {
                    'line-height': (newValue.h/1.5) + 'px',
                    'text-align' : 'center'
                        
                        
                };
            };

        }, true);

        // w.bind('resize', function () {
        //     scope.$apply();
        // });
    };
  });
