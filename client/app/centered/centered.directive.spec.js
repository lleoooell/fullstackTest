'use strict';

describe('Directive: centered', function () {

  // load the directive's module
  beforeEach(module('greenberryApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
