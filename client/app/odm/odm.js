'use strict';

angular.module('greenberryApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('odm', {
        url: '/odm?keyword',
        templateUrl: 'app/odm/odm.html',
        controller: 'OdmCtrl',
        resolve: {
          query: function(){return null;}
        }
      });
  });
