'use strict';

angular.module('greenberryApp')
    .controller('OdmCtrl', function($scope, $http, socket,query) {
        $scope.message = 'Hello';
        // Simple GET request example :
        $scope.busy = true;
        $scope.noMoreData = false;
        $http.get('/api/pictures',{params: {query: query}}).
        then(function(response) {
            // this callback will be called asynchronously
            // when the response is available
            console.info(response);
            // var toDisplay = _(response.data).reverse();

            $scope.photos = response.data;
            if ($scope.photos.length < 5) {
                $scope.noMoreData = true;
            };
            $scope.busy = false;
            socket.syncUpdatesReverse('picture', $scope.photos);
        }, function(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
            console.log(response);
        });



        $scope.nextPage = function() {
            if ($scope.busy) {
                return;
            }
            $scope.busy = true;
            var lastId = $scope.photos[$scope.photos.length - 1]._id;
            console.info(lastId);
            console.info(query);
            var pageQuery =  {
                _id: {
                    $lt: lastId
                }
            };
            console.info(pageQuery);
            $http.get('/api/pictures', {
                params: {
                    query: pageQuery
                }
            }).success(function(photos_) {
                console.log(photos_);
                $scope.photos = $scope.photos.concat(photos_);
                socket.syncUpdatesReverse('picture', $scope.photos);
                $scope.busy = false;
                if (photos_.length === 0) {
                    $scope.noMoreData = true;
                }
            });
        };
    });