'use strict';

describe('Controller: OdmCtrl', function () {

  // load the controller's module
  beforeEach(module('greenberryApp'));

  var OdmCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    OdmCtrl = $controller('OdmCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
