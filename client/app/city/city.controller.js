'use strict';

angular.module('greenberryApp')
    .controller('CityCtrl', function($scope, $rootScope, $window) {
    	// $scope.test = vizi.World;
        
    	$rootScope.heatLayer = "assets/data/ceeIRIS.geojson";
        $rootScope.heatProp = "properties.toViz";
        // console.log($scope.test);
        $scope.ceeBati = function(layer, properties){
        	$rootScope.heatLayer = layer;
        	$rootScope.heatProp = properties;
        	$rootScope.$emit();
        };
        $rootScope.lon = 48.6881323;
        $rootScope.toAnimate = false;
        $scope.change = function(input) {
            console.log(input);
            $rootScope.lon = input;
            $rootScope.toAnimate = !$rootScope.toAnimate;
        };
        $scope.message = 'Hello';
        $scope.notifyServiceOnChage = function() {
            console.log($scope.windowHeight);
        };

    });