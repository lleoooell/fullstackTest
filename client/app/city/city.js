'use strict';

angular.module('greenberryApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('city', {
        url: '/city',
        templateUrl: 'app/city/city.html',
        controller: 'CityCtrl'
      });
  });
