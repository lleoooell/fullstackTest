'use strict';

describe('Service: vizi', function () {

  // load the service's module
  beforeEach(module('greenberryApp'));

  // instantiate service
  var vizi;
  beforeEach(inject(function (_vizi_) {
    vizi = _vizi_;
  }));

  it('should do something', function () {
    expect(!!vizi).toBe(true);
  });

});
