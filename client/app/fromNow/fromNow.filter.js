'use strict';

angular.module('greenberryApp')
  .filter('fromNow', function () {
    return function (input) {
      return moment(input).fromNow();
    };
  });


