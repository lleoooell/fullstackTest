'use strict';

describe('Controller: MainCtrl', function() {

  // load the controller's module
  beforeEach(module('greenberryApp'));
  beforeEach(module('stateMock'));
  beforeEach(module('socketMock'));

  var MainCtrl;
  var scope;
  var state;
  var $httpBackend;

  // Initialize the controller and a mock scope
  beforeEach(inject(function(_$httpBackend_, $controller, $rootScope, $state) {
    $httpBackend = _$httpBackend_;
    $httpBackend.expectGET('/api/things')
      .respond(['HTML5 Boilerplate', 'AngularJS', 'Karma', 'Express']);

    scope = $rootScope.$new();
    state = $state;
    MainCtrl = $controller('MainCtrl', {
      $scope: scope,
      query: null,
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
